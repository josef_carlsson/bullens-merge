<?php get_header(); ?>

	<main role="main">
	<!-- section -->
	<section class="section span_12_of_12 contactWrapper">

	<?php if (have_posts()): while (have_posts()) : the_post();

		if(has_post_thumbnail()) : ?>
		<section class="span_6_of_12 txtFieldImgContainer--single"
		style="background-image:url('<?php the_post_thumbnail_url(); ?>');">
		</section>
	<?php endif; ?>
		<article class="span_6_of_12 txtFieldContainer">
			<h1 class="txtFieldHeader"><?php the_title(); ?></h1>
			<div class="txtField">
				<?php the_content(); ?>
			</div>
		</article>
	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>

</section> <!-- contactWrapper-->
	<!-- /section -->
	</main>


<?php get_footer(); ?>
