<?php
/* Template Name: Kontakt */

get_header(); ?>

	<main role="main" class="mainWrapper">
	<!-- section -->

<!-- top hero image section -->
<section class="section span_12_of_12 contactWrapper">
<?php
if (get_field('stycke')) :
	if(have_rows('stycke')) :
		while(have_rows('stycke')) : the_row();
		$header = get_sub_field('rubrik');
		$text = get_sub_field('textstycke');
		$img = get_sub_field('bild');



?>

		<section class="span_6_of_12 txtFieldImgContainer">
			<img src="<?php echo $img; ?>" class="txtFieldImg"/>
		</section>
		<section class="span_6_of_12 txtFieldContainer">
			<h2 class="txtFieldHeader"><?php echo $header; ?></h2>
			<div class="txtField">
				<?php echo $text; ?>
			</div>
		</section>






<?php
	endwhile;
	endif;
endif; ?>
	</section>

	</main>



<?php get_footer(); ?>
