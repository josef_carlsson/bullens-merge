<?php
/* Template Name: Historia */

get_header(); ?>

	<main role="main" class="mainWrapper">
	<!-- section -->
	<?php
	$sectionId = 0;
	?>
		<section class="grid pageContentWrapper--no-img span_12_of_12">
			<section class="span_6_of_12"></section>

	<?php
	$counter = 0;
	$totalSections = 0;
	$sectionId = 0;
	if(get_field('stycke')) :
		if(have_rows('stycke')) :
			while (have_rows('stycke')) : the_row();
				$header = get_sub_field('rubrik');
				$text = get_sub_field('textstycke');
				$img = get_sub_field('bild');
				$sectionId++;

	?>
	<section class="grid-item" id="<?php echo $sectionId; ?>">

			<?php
			if(get_sub_field('rubrik')) {
				?>
		<section class="txtContainer--history <?php
		if(get_sub_field('bild')) { print 'padding-top';}?>">
		  <h2 class="gridHeader"><?php echo $header; ?></h2>
			<div class="txtContent--history">
					<?php echo $text; ?>
			</div>
		</section>
				<?php
			}
			?>
			<?php
			if(get_sub_field('bild')) { ?>
		    <section class="imgContainer--history">
		      <img src="<?php echo $img; ?>" class="gridImg"/>
		    </section>
			<?php }
			?>
	</section>  <!-- grid -->






	<?php
			endwhile;
		endif;
	endif;
	?>
	</section> <!-- pageContentWrapper--no-img -->
	<!-- </section> -->


	</main>


<?php get_footer(); ?>
