<?php
/* Template Name: Superpriset vinstsida*/

get_header();

?>

<main role="main" class="">
  <section class="section span_12_of_12">
    <section class="heroContent--super">
      <div class="darkModal">


      <section class="heroContent--Txt " >
        <h1 class="heroHeader">Vinn en riktig korvfest</h1>
      </section>
      </div>
    </section>
  </section>
  <section class="section span_12_of_12 formwrapper">
    <div class="maxWidth">
      <section class="section span_12_of_12 formContainer--super">

          <h2 class="">Bullens food truck</h2>
          <p class="">Har du alltid drömt om en egen food truck fullastad med god gratis korv? Nu har du chansen. Bullens Food Truck turnerar just nu runt om i landet och du har chansen att göra din fest till ett av stoppen. För att vinna – berätta varför just din fest behöver en food truck fullastad med Bullens Beer Banger! Tävlingen pågår till och med den 19 april, men vänta inte utan tävla redan idag. Våra tävlingsregler hittar du <a href="http://c9019.cloudnet.cloud/regler/">här</a>.</p>
          <form action="" class=""><!-- super-price -->
            <div class="span_6_of_12 formSection">
                <div class="calendar">
                  <h5 class="formLabel">Välj datum</h5>
                  <select name="days" class="dropDown">
                    <option data-type="day" value="22/4" class="val">22 april</option>
                    <option data-type="day" value="29/4" class="val">29 april</option>
                    <option data-type="day" value="6/5" class="val">6 maj</option>
                    <option data-type="day" value="13/5" class="val">13 maj</option>
                  </select>
                </div>
                <h5 class="formLabel">Motivering</h5>
                <textarea class="val inputField--superPrize" data-type="motivation" placeholder="Motivering" name="motivation"></textarea>
            </div>
            <div class="span_6_of_12 formSection" id="infoForm">
              <h5 class="formLabel">Information</h5>
              <input type="text" class="val inputField--superPrize unvalid" data-type="name" placeholder="Förnamn Efternamn" />
              <input type="email" class="val inputField--superPrize" data-type="mail" placeholder="Mejladress" required/>
              <input type="number" class="val inputField--superPrize" data-type="phone" placeholder="Telefonnummer" required/>
              <input type="text" class="val inputField--superPrize unvalid" data-type="adress" placeholder="Adress" />
              <input type="text" class="val inputField--superPrize city unvalid" data-type="city" placeholder="Stad" />
              <input type="number" class="val inputField--superPrize zip" data-type="postcode" placeholder="Postnr" />
              <input type="submit" value="Skicka" class="btn--superPrize" id="btn--superPrize" />
            </div>
          </form>
        </section>
        <section class="section span_12_of_12 formConfirmation">
          <div>
            <h2>Tack för ditt bidrag</h2>
            <p>Nu är det bara att hålla tummarna. Vinnarna kommer att kontaktas den 13 eller den 20 april. <br />Vill du vara säker på att vinna? Snurra på Bullens korvhjul, ett hjul med bara gott.</p>
            <a href="http://c9019.cloudnet.cloud/korvhjulet/"><button class="btn--superPrize">Till hjulet</button></a>
          </div>

        </section>


        <!-- <button class="heroBtn" id="heroBtn-compete" style="display: block;">Tävla</button> -->
    </div>


  </section>


    <?php ?>

</main>

<?php get_footer(); ?>
