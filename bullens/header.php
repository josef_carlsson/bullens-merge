<?php include_once('includes/settings.php');?>
<!doctype html>

<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
    <!-- <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon"> -->
    <!-- <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed"> -->

		<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<script>
			window.siteURL = "<?php echo get_template_directory_uri(); ?>/";
		</script>
		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class=""> <!-- class="wrapper" -->

			<!-- header -->
			<header class="header" role="banner">

					<!-- nav -->
					<nav class="nav" role="navigation">
						<a href="<?php echo home_url(); ?>">
							<script type="text/javascript">
							  var directoryUri = '<?php echo get_template_directory_uri(); ?>';
								window.directoryUri;
							</script>
							<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->

						 <img src="<?php echo get_template_directory_uri(); ?>/img/logo/logo-bullens_140.png" alt="Logo" class="logo" />

						</a>
						<div class="mobile-menu" id="mobile-menu">
							<div class="bar-menu"></div>
							<div class="bar-menu"></div>
							<div class="bar-menu"></div>
						</div>

						<?php html5blank_nav(); ?>
					</nav>
					<!-- /nav -->

			</header>
			<!-- /header -->
