<?php get_header(); ?>

	<main role="main">
		<section class="section span_12_of_12 contactWrapper">
				<article id="post-404" class="heroContent--Txt x-padding-top">
					<h2 class="subHeader">Å nej, den sidan verkar inte finnas. Välj något i menyn eller <a href="<?php echo home_url(); ?>"><?php _e( 'gå tillbaka till start', 'html5blank' ); ?></a>. </h2>
				</article>
		</section>
	</main>


<?php get_footer(); ?>
