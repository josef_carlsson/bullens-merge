<?php

/* Template Name: Lyckohjulet  */

get_header();
$couponID = getTypeOfCoupon();
//$couponID = '';
?>
  <main role="main" class="mainWrapper">
		<section class="section span_12_of_12 heroCampaignWrapper">
<?php
  if(get_field('toppbild')) :
    $heroImg = get_field('toppbild');
  	if(get_field('huvudrubrik')) :
  		$heroHeader = get_field('huvudrubrik');
  	endif;
  	if(get_field('text')):
  		$heroTxt = get_field('text');
  	endif;
  endif;
?>
			<section class="heroCampaign" style="background-image:url('<?php echo $heroImg; ?>');"></section><!-- -->
			<section class="heroContent" id="heroContent">
				<section class="heroContent--Txt" >

            <?php
            if($couponID == 'slut') { ?>
              <h1 class="heroHeader">Dagens kuponger är slut</h1>
              <p>Korvhjulet är öppet varje vardag mellan kl 09:00 och 18.00 eller tills kupongerna tar slut. När korvhjulet är öppet ger vi bort 200 smakprover och 200 <em>två för en-kuponger</em>.</p>
              <p>Men du har väl inte missat att du kan tävla och vinna en riktig korvfest med Bullens Food Truck <a href="http://c9019.cloudnet.cloud/foodtruck/">här</a>.</p>
            <?php } else { ?>
              <h1 class="heroHeader">
              <?php echo $heroHeader;?>
              </h1>
              <p>
                <?php echo $heroTxt; ?>
              </p>
              <button class="heroBtn" id="place-in-queue">Till hjulet</button>
          <?php  } ?>
				</section>
			</section>
      <section class="section span_12_of_12 videoQueueContainer">
        <section class="videoContainer">
          <iframe class="videoPlayer" src="http://c9019.cloudnet.cloud/video/" allowfullscreen="" scrolling="no"> <!--width="400" height="300"-->
        <p>Your browser does not support iframes.</p>
          </iframe>


  			</section>
        <div class="prizeContainer section span_12_of_12"><!--style="display:none;-->
          <section class="section span_12_of_12 heroContent--Txt queueInfo">
            <section class="">
              <div id="livestream"></div>
            </section>

          </section>
        </div>
      </section>


		</section>


<?php ?>
		<section class="section puffWrapper span_12_of_12">
		<?php
		$counter = 0;
		if(get_field('puff')) :
			if(have_rows('puff')) :
				while (have_rows('puff')) : the_row();
				$title = get_sub_field('rubrik');
				$text = get_sub_field('text');
				$subFieldImg = get_sub_field('bild');
				$img = $subFieldImg;
				if(get_sub_field('lank')) :
					$link = get_sub_field('lank');
				endif;
				if(get_sub_field('lanktext')) :
				  $linkTxt = get_sub_field('lanktext');
					else :
					$linkTxt = 'Läs mer';
				endif;
				if($counter == 0) {
					$imgClass = 'imgRight';
					$counter++;
				} elseif ($counter == 1) {
					$imgClass = 'imgLeft';
					$counter = 0;
			}
				?>

				<section class="section span_6_of_12 puffTxtContainer">
					<div class="puffContent">
						<h2 class="puffHeader"><?php echo $title;?></h2>
						<article class="puffDivider">
							<?php echo $text;
							if(get_sub_field('lank')) :
							?>
								<span><a href="<?php echo $link; ?>" ><?php echo $linkTxt; ?> </a></span>
							<?php endif; ?>
						</article>
					</div>
				</section>
				<section class="section span_6_of_12 puffImgContainer <?php echo $imgClass; ?>" style="background-image:url('<?php echo $img; ?>');"></section>
				<?php endwhile;
			endif;
		endif;
		?>
		</section>
	</main>

<?php get_footer(); ?>
