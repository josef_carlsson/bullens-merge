<?php $title = "";
      $brand = "";
      $files = array();
      $output_dir = realpath(__DIR__ . '/..') . "/uploads/";
      $thankyou = false;
      $editEntry = false;
      $hasFiles = false;

      $registerDone = false;

      if(!empty($_POST)){
        $errors = array();
        $title = trim($_POST['title']);
        $brand = trim($_POST['brand']);
        $userid = (int)$_SESSION[$sessionPrefix . '-id'];
        $newOrUpdate = trim($_POST['new-or-update']);
        $newOrUpdate = filter_var($newOrUpdate, FILTER_SANITIZE_SPECIAL_CHARS);
        $code = trim($_POST['code']);
        $code = filter_var($code, FILTER_SANITIZE_SPECIAL_CHARS);
        $entryid = trim($_POST['entryid']);
        $entryid = filter_var($entryid, FILTER_SANITIZE_SPECIAL_CHARS);

        if($userid < 1){
            $errors[] = "Något gick fel.";
        }

        if(empty($title)){
            $errors[] = 'Var vänlig fyll i bidragets titel.';
        } else{
            $title  = filter_var($title, FILTER_SANITIZE_SPECIAL_CHARS);
        }

        if(empty($brand)){
            $errors[] = 'Var vänlig fyll i varumärke/kund.';
        } else{
            $brand = filter_var($brand, FILTER_SANITIZE_SPECIAL_CHARS);
        }

        $validExtensions = array('.jpg', '.jpeg, .pdf, .png, .gif, .mov, .avi, .mp4, .txt, .doc, .wav, .mp3, .docx');

        //var_dump($_FILES);
        if(!empty($_FILES['ie9file'])){
            //var_dump($_FILES['ie9file']['name']);
           /* if(!is_array($_FILES["ie9file"]["name"])) //single file
            {
                print 2;
                if($_FILES["ie9file"]["error"] > 0){
                    $errors[] = "Det blev fel vid filöverföringen.";
                }

                //if(checkFileExt()

            }
            else  //Multiple files, file[]
            {*/
              $fileCount = count($_FILES["ie9file"]["name"]);
              for($i=0; $i < $fileCount; $i++)
              {
                if(!empty($_FILES["ie9file"]["name"][$i])){
                    $hasFiles = true;
                } 

                if( $_FILES["ie9file"]["error"][$i] > 0 && strlen($_FILES["ie9file"]["name"][$i]) > 0 ){
                    $errors[] = "Det blev fel vid filöverföringen.";
                }

                $fileExtension = strrchr($_FILES['ie9file']['name'][$i], ".");
                if(!checkFileExt($fileExtension)){
                    $extentionsText = "";
                    foreach($validExtensions as $extention){
                        $extentionsText = $extentionsText . $extention . " ";
                    }
                    $errors[] = "Filformatet stöds inte – tillgängliga filformat är " . $extentionsText;
                }

                if($_FILES['ie9file']['size'][$i] > (150 * 1048576)){
                    $errors[] = "Filen " . $_FILES['ie9file']['name'][$i] . " är för tung." . $_FILES['ie9file']['size'][$i];
                }
              }   
            //}
        }

        if(empty($errors)){
            if($newOrUpdate === 'new'){
                $data = registerEntry($title, $brand, $userid);
                $registerDone = true;
                //$code = $_POST['code'];
                $entryid = $data['entryid'];
            } else{
                $data = updateEntry($title, $brand, $userid, $entryid, $code);
                $registerDone = true;
            }

            if($entryid > 0 && $hasFiles){
                for($i=0; $i < $fileCount; $i++){
                    $origname = $_FILES["ie9file"]["name"][$i];
                    if(!empty($origname)){
                        $fileName = $entryid . "-" . time() . "-" . $_FILES["ie9file"]["name"][$i];
                        registerFile($fileName, $entryid, $origname);
                        move_uploaded_file($_FILES["ie9file"]["tmp_name"][$i],$output_dir.$fileName);
                    }
                }
            }
            
            $thankyou = true;

            
        }

      } else if(!empty($_GET) && (int)$_GET['entryid'] > 0 && strlen($_GET['entrycode']) === 25 && isset($_SESSION) && isset($_SESSION[$sessionPrefix . '-id']) && (int)$_SESSION[$sessionPrefix . '-id'] > 0){

                $entryid = filter_var($_GET['entryid'], FILTER_SANITIZE_SPECIAL_CHARS);
                $entryid = (int)$entryid;
                $code = filter_var($_GET['entrycode'], FILTER_SANITIZE_SPECIAL_CHARS);
                $userid = (int)$_SESSION[$sessionPrefix . '-id'];
                $entry = getEntryForEdit($userid, $code, $entryid);

                $title = htmlspecialchars($entry[0]['entrytitle']);
                $brand = htmlspecialchars($entry[0]['brand']);

                //var_dump($entry);

                $editEntry = true;

              }