<?php
//include('phpass/PasswordHash.php');
$success = false;
$data = array();


//*************** queue ****************/

function updateWinner($phone, $code){
    $query = "UPDATE winner SET phone = :phone WHERE code = :code";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':code', $code);
    $PDO->bindParam( ':phone', $phone);
    $success = $PDO->execute();

}
function createCoupon($phone){
	
	 $couponID = getTypeOfCoupon();
	 $create = new stdClass();
	 $create->msisdn = $phone;
	 $create->couponId = $couponID;
	 $create->identifier = $_SESSION['winnercode'];
	 $createUrl = 'http://api.kupong.se/v1.5/Coupons';
	
	 $headers = array(
	      'Content-type: application/json',
	      'Accept : application/json',
	      'Authorization: Basic MmNlMTI5NThiYzljNDQ3MjgyNzY1MTUyYmU0ODFlYmQ6dWZ5ZnVsdG4=',
	  );
	
	  $process = curl_init();
	  curl_setopt($process, CURLOPT_URL, $createUrl);
	  curl_setopt($process, CURLOPT_RETURNTRANSFER, 1 );
	  curl_setopt($process, CURLOPT_POST, 1); 
	  curl_setopt($process, CURLOPT_POSTFIELDS, json_encode($create));
	  curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
	  $return = curl_exec($process);
	  $response = json_decode($return, true );
	  if($response['couponCode']){
		  sendCoupon($response, $couponID);
		  return true;
	  }
	  else{
		  return false;
	  }
	  
	  curl_close($process);
}
function getNumberOfCoupon(){
	 $query = "SELECT COUNT(*) FROM numberOfcoupon WHERE DATE(created) = CURDATE()";
	 $PDO = Database::$connection->prepare( $query );
     $PDO->execute();
     while ($row = $PDO->fetch(PDO::FETCH_ASSOC)) {
        $numberOfCoupon = $row["COUNT(*)"];
     }
     return $numberOfCoupon;
    
}
function getNumberInQueue(){
	 $query = "SELECT COUNT(*) FROM queue";
	 $PDO = Database::$connection->prepare( $query );
     $PDO->execute();
     while ($row = $PDO->fetch(PDO::FETCH_ASSOC)) {
        $sizeOfqueue = $row["COUNT(*)"];
     }
	 return $sizeOfqueue;
}
function getTypeOfCoupon(){
	$numberOfCupongs = getNumberOfCoupon() + getNumberInQueue();
	if($numberOfCupongs >= 200){
		$couponID = '949';
		if($numberOfCupongs >= 400){
			$couponID = 'slut';
			}
	}
	else{
		$couponID = '948';
	}
	return $couponID;
}
function sendCoupon($response, $couponID){
	 $sendUrl = 'http://api.kupong.se/v1.5/Coupons/Send';
	 $headers = array(
	      'Content-type: application/json',
	      'Accept : application/json',
	      'Authorization: Basic MmNlMTI5NThiYzljNDQ3MjgyNzY1MTUyYmU0ODFlYmQ6dWZ5ZnVsdG4=',
	  );
	
	  $send = new stdClass();
	  $send->couponCode = $response['couponCode'];
	  $send->identifier = $_SESSION['winnercode'];
	  $send->sendOn = "";
	  $send->smsTemplate = "";
	  
	  $process = curl_init();
	  curl_setopt($process, CURLOPT_URL, $sendUrl);
	  curl_setopt($process, CURLOPT_RETURNTRANSFER, 1 );
	  curl_setopt($process, CURLOPT_POST, 1); 
	  curl_setopt($process, CURLOPT_POSTFIELDS, json_encode($send));
	  curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
	  $return = curl_exec($process);
	  curl_close($process);  
	  
	  $query = "INSERT INTO numberOfcoupon (coupon) VALUES (:coupon)";
	  $PDO = Database::$connection->prepare( $query );
	  $PDO->bindParam( ':coupon', $couponID);
	  $success = $PDO->execute();
}

function registerInQueue($code, $browser){
    $query = "INSERT INTO queue (code, browser) VALUES (:code, :browser)";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':code', $code);
    $PDO->bindParam( ':browser', $browser);
	$success = $PDO->execute();
    if($success){
        return $code;
    } else{
        return false;
    }
}
function getPlaceInQueue(){
	$query = "SELECT * FROM queue";
    $PDO = Database::$connection->prepare( $query );
    $PDO->execute();
    $results = $PDO->fetchAll( PDO::FETCH_ASSOC );
      foreach( $results as $key => $row ){
	      	if($row['code'] == $_SESSION["code"]){
		      	updateCheckTime($_SESSION["code"], $key);
			  	return $key;
		      	break;
	      	}
        }
}
function SuperFormSubmit($code, $day, $motivation, $name, $phone, $adress, $city, $postcode){
	$query = "INSERT INTO suprePrice (code, motivation, name, phone, adress, city, postcode, day) VALUES (:code , :motivation , :name , :phone , :adress , :city , :postcode, :day)";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':code', $code);
    $PDO->bindParam( ':motivation', $motivation);
    $PDO->bindParam( ':name', $name);
    $PDO->bindParam( ':day', $day);
    $PDO->bindParam( ':phone', $phone);
    $PDO->bindParam( ':adress', $adress);
    $PDO->bindParam( ':city', $city);
    $PDO->bindParam( ':postcode', $postcode);
    
	$success = $PDO->execute();
    if($success){
        return $code;
    } else{
        return false;
    }
}

function removeMeQueue($code){
	$query = "DELETE FROM queue WHERE code = :code";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':code', $code);
    $success = $PDO->execute();
    return $success;
}

function formSubmit($code, $phone){
	$query = "INSERT INTO suprePrice (code, motivation, name, phone, adress, city, postcode, day) VALUES (:code , :motivation , :name , :phone , :adress , :city , :postcode, :day)";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':code', $code);
    $PDO->bindParam( ':price', $code);
    $PDO->bindParam( ':phone', $phone);
    
	$success = $PDO->execute();
    if($success){
        return $code;
    } else{
        return false;
    }
}
function updateCheckTime($code, $interval){
	 $interval = $interval * 500;
	 if($interval > 15000){
		 $interval = 15000;
	 };
	 if($interval < 1000){
		 $interval = 1000;
	 };
	 
	 $query = "UPDATE queue SET created = now(), currentInterval = :interval WHERE code = :code";
	 $PDO = Database::$connection->prepare( $query );
	 $PDO->bindParam( ':code', $code);
	 $PDO->bindParam( ':interval', $interval);	
	 $success = $PDO->execute();
}

function removeFromQueue($code, $browser){
    //$query = "INSERT INTO queue (code, browser) VALUES (:code, :browser)";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':code', $code);
    $PDO->bindParam( ':email', $email);
    $PDO->bindParam( ':browser', $browser);
    $success = $PDO->execute();
}

function removeOldQueue(){
    $query = "DELETE FROM queue WHERE created < 1400";
    $PDO = Database::$connection->prepare( $query );
    $success = $PDO->execute();
}

function turnOffMotor(){
    $query = "UPDATE motor SET isMotorRunning = 0 WHERE id = 1";
    $PDO = Database::$connection->prepare( $query );
    $success = $PDO->execute();
}

function turnOnMotor($ip, $code, $userId){
    $query = "UPDATE motor SET isMotorRunning = 1, ip = :ip, code = :code, userId = :userId WHERE id = 1";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':ip', $ip);
    $PDO->bindParam( ':code', $code);
    $PDO->bindParam( ':userId', $userId);
    $success = $PDO->execute();
    return $success;
}

function checkMotor(){
    $query = "SELECT * FROM motor WHERE id = 1";
    $PDO = Database::$connection->prepare( $query );
    $PDO->execute();
    $results = $PDO->fetchAll( PDO::FETCH_ASSOC );
      foreach( $results as $row ){
            $response = array('isMotorRunning' => $row['isMotorRunning'], 'success' => true);
            return $response;
        }
      }

function createWinner($ip, $code){
	$query = "INSERT INTO winner (code, ip) VALUES (:code, :ip)";
	$PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':ip', $ip);
    $PDO->bindParam( ':code', $code);
    $success = $PDO->execute();
    $id = Database::$connection->lastInsertId();
    return $id;
    
}

function checkIfPriceIsValid($userId, $code, $ip){
	$query = "SELECT * FROM winner WHERE id = :id  AND ip = :ip AND code = :code";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':ip', $ip);
    $PDO->bindParam( ':id', $userId);
    $PDO->bindParam( ':code', $code); 
    $PDO->execute();
    $results = $PDO->fetchAll( PDO::FETCH_ASSOC );
    if($results){
	      foreach( $results as $row ){
            $response = array('winning' => $row['winning'], 'collected' => $row['phone'], 'success' => true);
            return $response;
        }
    }
    else{
	    return false;
    }
}
function collectWinning($ip, $code, $id){
	$query = "SELECT query FROM winner WHERE id = :id AND code = :code AND ip = :ip";
	$PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':id', $id);
    $PDO->bindParam( ':ip', $ip);
    $PDO->bindParam( ':code', $code);
    $PDO->execute();
    $results = $PDO->fetch( PDO::FETCH_ASSOC );
    
    return $results;
    
}

function getCode($length = 50) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //Membership
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

function registerMember($firstname, $lastname, $email, $password, $code, $isSFEmployee, $sfEmployeeNumber, $bioklubbenNumber){
    $query = "INSERT INTO members (firstname, lastname, email, pword, code, sfemployee, employeenr, bioklubben) VALUES (:firstname, :lastname, :email, :pword, :code, :sfemployee, :employeenr, :bioklubben)";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':firstname', $firstname);
    $PDO->bindParam( ':lastname', $lastname);
    $PDO->bindParam( ':email', $email);
    $PDO->bindParam( ':pword', $password);
    $PDO->bindParam( ':code', $code);
    $PDO->bindParam( ':sfemployee', $isSFEmployee);
    $PDO->bindParam( ':employeenr', $sfEmployeeNumber);
    $PDO->bindParam( ':bioklubben', $bioklubbenNumber);
    $success = $PDO->execute();
}

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //sf employ
  function isSFEmployee($email, $sfEmployeeNumber){
    $query = "SELECT COUNT(*) FROM sfemploye WHERE email=:email AND anstnr = :anstnr LIMIT 1";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':email', $email);
    $PDO->bindParam( ':anstnr', $sfEmployeeNumber);
    $PDO->execute();
    while ($row = $PDO->fetch(PDO::FETCH_ASSOC)) {
        $code_count = $row["COUNT(*)"];
    }

    if ($code_count > 0) {
       return true;
    } else{
       return false;
    }
}
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //Entries
//*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

function registerEntry($idea, $title, $category, $youtubeID, $code, $sessionPrefix){
  if(isset($_SESSION[$sessionPrefix . '-id']) && $_SESSION[$sessionPrefix . '-id'] > 0){
    $memberID = $_SESSION[$sessionPrefix . '-id'];
    $isDoubleCode = isDoubleCode($code, $memberID);
    $excerpt = substr ( $idea , 0 , 150 );

    if(!$isDoubleCode && $_SESSION[$sessionPrefix . '-loggedin'] && $memberID > 0){
        $query = "INSERT INTO entries (memberid, excerpt, title, category, youtubeid, code) VALUES (:memberid, :excerpt, :title, :category, :youtubeid, :code)";
        $PDO = Database::$connection->prepare( $query );
        $PDO->bindParam( ':memberid', $memberID);
        $PDO->bindParam( ':excerpt', $excerpt);
        $PDO->bindParam( ':title', $title);
        $PDO->bindParam( ':category', $category);
        $PDO->bindParam( ':youtubeid', $youtubeID);
        $PDO->bindParam( ':code', $code);
        $success = $PDO->execute();

        if($success){
            $lastID =  Database::$connection->lastInsertId('entries');
            $returnData;
            $returnData['lastid'] = $lastID;
            $returnData['code'] = $code;
            registerMetaDataFile($lastID);
            registerDescription($lastID, $idea);
            return $returnData;
        } else{
            return false;
        }

    }  else{
        $arr=array('status'=>'fail','message'=>'Koden är redan registrerad');
        echo json_encode($arr);
        exit();
    }
  }
}

function registerMetaDataFile($entryID){
    $comments = 0;
    $votes = 0;
    $query = "INSERT INTO entriesmetadata (entryid, comments, votes) VALUES (:entryid, :comments, :votes)";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':entryid', $entryID);
    $PDO->bindParam( ':comments', $comments);
    $PDO->bindParam( ':votes', $votes);
    $success = $PDO->execute();
}

function registerDescription($entryID, $description){
    $query = "INSERT INTO descriptions (entryid, description) VALUES (:entryid, :description)";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':entryid', $entryID);
    $PDO->bindParam( ':description', $description);
    $success = $PDO->execute();
}

function loginMember($email, $password, $sessionPrefix){
    $email = strtolower($email);

    $query = "SELECT * FROM members WHERE email=:email";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':email', $email);
    $PDO->execute();

    $results = $PDO->fetchAll( PDO::FETCH_ASSOC );
    if(isLogInOK($results[0][id])){
      if(empty($results)){
        $arr=array('status'=>'empty','message'=>'Mailadressen är inte registrerad.');
        echo json_encode($arr);
        exit();
      }
      exit();
      foreach( $results as $row ){
        $hash = $row['pword'];
        if (password_verify($password, $hash)) {
            // Login successful.

            if (password_needs_rehash($hash, PASSWORD_DEFAULT, ['cost' => 12])) {
              // Recalculate a new password_hash() and overwrite the one we stored previously
            }

            setSession($row, $sessionPrefix);

            $response = array('name' => $row['name'], 'success' => true);
            return $response;
        } else {
            $response = array('success' => false);
            return $response;
        }
      }
    }
    else{

      $arr=array('status'=>'error','message'=>'Fel lösenord för många gånger');
      echo json_encode($arr);
      exit();
    }
}

function isDoubleCode($code, $memberid){
    $query = "SELECT COUNT(*) FROM entries WHERE code = :code AND memberid = :memberid LIMIT 1";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':code', $code );
    $PDO->bindParam( ':memberid', $memberid );
    $PDO->execute();

    while ($row = $PDO->fetch(PDO::FETCH_ASSOC)) {
        $code_count = $row["COUNT(*)"];
    }

    if ($code_count > 0) {
       return true;
    } else{
       return false;
    }
}

function isDoubleEmail($email){
    $query = "SELECT COUNT(*) FROM members WHERE email = :email LIMIT 1";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':email', $email );
    $PDO->execute();

    while ($row = $PDO->fetch(PDO::FETCH_ASSOC)) {
        $row_count = $row["COUNT(*)"];
    }

    if ($row_count > 0) {
       return true;
    } else{
       return false;
    }
}

function registerComment($name, $fbid, $comment, $entryid, $commenttype){
    $img = 'https://graph.facebook.com/' . $fbid . '/picture?type=large';
    $code = getRandomString();
    if($commenttype == 'blog'){
        $query = "INSERT INTO blogcomments (name, fbid, comment, entryid, img, code) VALUES (:name, :fbid, :comment, :entryid, :img, :code)";
    } else{
        $query = "INSERT INTO comments (name, fbid, comment, entryid, img, code) VALUES (:name, :fbid, :comment, :entryid, :img, :code)";
    }

    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':name', $name);
    $PDO->bindParam( ':fbid', $fbid);
    $PDO->bindParam( ':comment', $comment);
    $PDO->bindParam( ':entryid', $entryid, PDO::PARAM_INT );
    $PDO->bindParam( ':img', $img);
    $PDO->bindParam( ':code', $code);
    $success = $PDO->execute();
    $lastID =  Database::$connection->lastInsertId('comments');

    $t=time();
    $returnData = array('img' => $img, 'name' => $name, 'comment'=> $comment, 'time' => date("Y-m-d, H:i",$t), 'id'=>$lastID);

    if($success){
        return $returnData;
    } else{
        return false;
    }
}

function registerAnswer($name, $fbid, $comment, $entryid, $answerTo, $commenttype){
    $img = 'https://graph.facebook.com/' . $fbid . '/picture?type=large';
    $code = getRandomString();
    if($commenttype == 'blog'){

        $query = "INSERT INTO bloganswers (name, fbid, comment, entryid, commentid, code) VALUES (:name, :fbid, :comment, :entryid, :answerto, :code)";
    } else{

        $query = "INSERT INTO answers (name, fbid, comment, entryid, commentid, code) VALUES (:name, :fbid, :comment, :entryid, :answerto, :code)";
    }
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':name', $name);
    $PDO->bindParam( ':fbid', $fbid);
    $PDO->bindParam( ':comment', $comment);
    $PDO->bindParam( ':entryid', $entryid);
    $PDO->bindParam( ':answerto', $answerTo);
    $PDO->bindParam( ':code', $code);
    $success = $PDO->execute();
    $lastID =  Database::$connection->lastInsertId('answers');

    $t=time();
    $returnData = array('img' => $img, 'name' => $name, 'comment'=> $comment, 'time' => date("Y-m-d, H:i",$t), 'id' => $lastID);

    if($success){
        return $returnData;
    } else{
        return false;
    }

}
function isLogInOK($id){
  $query = "SELECT * FROM loginsession WHERE id=:id";
  $PDO = Database::$connection->prepare( $query );
  $PDO->bindParam( ':id', $id);
  $PDO->execute();
  $result = $PDO->fetchAll();
  $updated = strtotime($result[0]->updated);
  $tries = $result[0]->times;
  $curtime = time();


  if(($curtime - $updated) < 360){
    createSession($id, $tries, $result);
  }
  else{
    $tries = 0;
    createSession($id, $tries, $result);
  }

  if($tries < 4){
      return true;
  }
  else{
      return false;
  }
}

function createSession($id, $tries, $result){
  if($result){
    $times = $tries + 1;
    $query = "UPDATE loginsession SET times=:times, updated=now() WHERE id=:id";
  }
  else{
    $times = 0;
    $query = "INSERT INTO loginsession (id, times) VALUES (:id, :times)";
  }

  $PDO = Database::$connection->prepare( $query );
  $PDO->bindParam( ':id', $id);
  $PDO->bindParam( ':times', $times);
  $success = $PDO->execute();
  return $times;
}

function getRandomString(){
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < 25; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }

    return $randomString;
}

//Ta bort?
function registerFile($filename, $entryid, $origname){
    $randomString = getRandomString();
    $query = "INSERT INTO files (entryid, filename) VALUES (:entryid, :name)";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':name', $filename);
    $PDO->bindParam( ':entryid', $entryid);
    $success = $PDO->execute();

    return $success;
}


function regImgCompEntry($fileName, $lastID){
        $query = "INSERT INTO files (filename, entryid) VALUES (:img, :id)";
        $PDO = Database::$connection->prepare( $query );
        $PDO->bindParam( ':img', $fileName);
        $PDO->bindParam( ':id', $lastID, PDO::PARAM_INT);

    $success = $PDO->execute();

    //$lastID =  Database::$connection->lastInsertId('users');

}

function checkIMG($file, $fileName, $output_dir){
    $upload = $_FILES["myfile"];
    if($upload['error'] > 0){
        return false;
    } else {
    // array of valid extensions
    $validExtensions = array('.jpg', '.jpeg', '.gif', '.png', '.JPG', '.JPEG', '.PNG', '.GIF');
    // get extension of the uploaded file
    $fileExtension = strrchr($file['name'], ".");


    // check if file Extension is on the list of allowed ones
        if (in_array($fileExtension, $validExtensions)) {

           if($fileExtension == '.jpg' || $fileExtension == '.JPG' || $fileExtension == '.jpeg' || $fileExtension == '.JPEG'){
                $rotate = new imageRotate();
                $rotate->fixOrientation($file['tmp_name']);
           }
            $manipulator = new ImageManipulator($file['tmp_name']);

            $width  = $manipulator->getWidth();
            $height = $manipulator->getHeight();

            if($width <  1 || $height < 0){
                return false;
            }

            if($width > 2500){
                $ratio = 2500 / $width;
                $newHeight = $ratio * $height;
                $manipulator->resample(2500, $newHeight);
            }

            $width  = $manipulator->getWidth();
            $height = $manipulator->getHeight();

            if($height > 2000){
                $ratio = 2000 / $height;
                $newWidth = $ratio * $width;
                $manipulator->resample($newWidth, 2000);
            }

            //$newImage = $manipulator->crop($x1, $y1, $x2, $y2);
            // saving file to uploads folder
            $manipulator->save($output_dir . $fileName);
            //return $newNamePrefix . $_FILES['img']['name'];
        } else {
            return false;
            }
        }
    }


function regFiles($lastID, $files, $output_dir){
    //$userid = (int)$profile[0]['id'];

    //registerFile($_POST['name']);
    $ret = array();
    $files = $files['myfile'];
    $error =$files["error"];

    if($error > 0){
        return false;
    }

    //You need to handle both cases
    //If Any browser does not support serializing of multiple files using FormData()
    if(!is_array($files["name"])) //single file
    {
        $randint = rand( 1 , 100000 );
        //$origname = $_FILES["myfile"]["name"];
        $fileExtension = strrchr($files["name"], ".");
        $fileName = $lastID . '-' . $randint . '-' . time() . "-img" . $fileExtension;
        //move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$fileName);
        checkIMG($files, $fileName, $output_dir);
        regImgCompEntry($fileName, $lastID);

    }
    else  //Multiple files, file[]
    {
      $fileCount = count($files["name"]);
      for($i=0; $i < $fileCount; $i++)
      {
        //$randint = rand( 1 , 10000 );
        $origname = $files[$i]["name"];
        $fileExtension = strrchr($files[$i]["name"], ".");
        $fileName = $lastID . '-' . $randint . '-' . time() . "-img" . $fileExtension;
        checkIMG($files[$i], $fileName, $output_dir);
        regImgCompEntry($fileName, $lastID);

      }

    }
}

function loginUser($email, $password, $sessionPrefix){
    $email = strtolower($email);
    $query = "SELECT * FROM users WHERE email=:email";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':email', $email);
    $PDO->execute();
    $results = $PDO->fetchAll( PDO::FETCH_ASSOC );

    foreach( $results as $row ){
        $hash_obj = new PasswordHash( 8, false );
        $stored_hash = $row['password'];

        $check = $hash_obj->CheckPassword( $password, $stored_hash );

        if ( $check ){
            setSession($row, $sessionPrefix);
            $response = array('name' => $row['name'], 'success' => true);
            return $response;
        } else {
            $response = array('success' => false);
            return $response;
        }
    }
}


function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP'])
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']) && $_SERVER['HTTP_X_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']) && $_SERVER['HTTP_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']) && $_SERVER['HTTP_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'])
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 0;
    return $ipaddress;
}

function isVoteOK($uid, $entryID){
    $query = "SELECT COUNT(*) FROM votes WHERE fbid = :fbid AND created > DATE_SUB(now(), INTERVAL 1 DAY) AND entryid = :entryid LIMIT 1";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':fbid', $uid, PDO::PARAM_INT );
    $PDO->bindParam( ':entryid', $entryID, PDO::PARAM_INT );
    $PDO->execute();

    while ($row = $PDO->fetch(PDO::FETCH_ASSOC)) {
        $vote_count = $row["COUNT(*)"];
    }

    if (isset($vote_count) && $vote_count > 0) {
       return false;
    } else{
       return true;
    }
}

function isBlogVoteOK($uid, $entryID){
    $query = "SELECT COUNT(*) FROM blogvotes WHERE fbid = :fbid AND entryid = :entryid LIMIT 1";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':fbid', $uid, PDO::PARAM_INT );
    $PDO->bindParam( ':entryid', $entryID, PDO::PARAM_INT );
    $PDO->execute();

    while ($row = $PDO->fetch(PDO::FETCH_ASSOC)) {
        $vote_count = $row["COUNT(*)"];
    }

    if (isset($vote_count) && $vote_count > 0) {
       return false;
    } else{
       return true;
    }
}

function isEntryCommentVoteOK($uid, $entryID, $commentid){
    $query = "SELECT COUNT(*) FROM commentvotes WHERE fbid = :fbid AND commentid = :commentid LIMIT 1";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':fbid', $uid, PDO::PARAM_INT );
    $PDO->bindParam( ':commentid', $commentid, PDO::PARAM_INT );
    $PDO->execute();

    while ($row = $PDO->fetch(PDO::FETCH_ASSOC)) {
        $vote_count = $row["COUNT(*)"];
    }

    if (isset($vote_count) && $vote_count > 0) {
       return false;
    } else{
       return true;
    }
}

function isEntryAnswerVoteOK($uid, $entryID, $commentid, $answerid){
    $query = "SELECT COUNT(*) FROM answervotes WHERE fbid = :fbid AND entryid = :entryid AND answerid = :answerid LIMIT 1";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':fbid', $uid, PDO::PARAM_INT );
    $PDO->bindParam( ':entryid', $entryID, PDO::PARAM_INT );
    $PDO->bindParam( ':answerid', $answerid, PDO::PARAM_INT );
    $PDO->execute();

    while ($row = $PDO->fetch(PDO::FETCH_ASSOC)) {
        $vote_count = $row["COUNT(*)"];
    }

    if (isset($vote_count) && $vote_count > 0) {
       return false;
    } else{
       return true;
    }
}

function isBlogCommentVoteOK($uid, $entryID, $commentid){
    $query = "SELECT COUNT(*) FROM blogcommentvotes WHERE fbid = :fbid AND entryid = :entryid AND commentid = :commentid LIMIT 1";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':fbid', $uid, PDO::PARAM_INT );
    $PDO->bindParam( ':entryid', $entryID, PDO::PARAM_INT );
    $PDO->bindParam( ':commentid', $commentid, PDO::PARAM_INT );
    $PDO->execute();

    while ($row = $PDO->fetch(PDO::FETCH_ASSOC)) {
        $vote_count = $row["COUNT(*)"];
    }

    if (isset($vote_count) && $vote_count > 0) {
       return false;
    } else{
       return true;
    }
}

function isBlogAnswerVoteOK($uid, $entryID, $commentid, $answerid){
    $query = "SELECT COUNT(*) FROM bloganswervotes WHERE fbid = :fbid AND entryid = :entryid AND answerid = :answerid LIMIT 1";
    $PDO = Database::$connection->prepare( $query );
    $PDO->bindParam( ':fbid', $uid, PDO::PARAM_INT );
    $PDO->bindParam( ':entryid', $entryID, PDO::PARAM_INT );
    $PDO->bindParam( ':answerid', $answerid, PDO::PARAM_INT );
    $PDO->execute();

    while ($row = $PDO->fetch(PDO::FETCH_ASSOC)) {
        $vote_count = $row["COUNT(*)"];
    }

    if (isset($vote_count) && $vote_count > 0) {
       return false;
    } else{
       return true;
    }
}

function registerVote($entryid, $ip, $browser, $fbid, $voteType, $commentid, $answerid){
    if($entryid < 1){
        return false;
    } else{
        switch ($voteType) {
        case 'blogcomment':
            $query = "INSERT INTO blogcommentvotes ( entryid, ip, browser, fbid, commentid ) VALUES ( :entryid, :ip, :browser, :fbid, :commentid)";
            $PDO = Database::$connection->prepare( $query );
            $PDO->bindParam( ':commentid', $commentid, PDO::PARAM_INT );
            break;
        case 'bloganswer':
            $query = "INSERT INTO bloganswervotes ( entryid, ip, browser, fbid, commentid, answerid ) VALUES ( :entryid, :ip, :browser, :fbid, :commentid, :answerid)";
            $PDO = Database::$connection->prepare( $query );
            $PDO->bindParam( ':commentid', $commentid, PDO::PARAM_INT );
            $PDO->bindParam( ':answerid', $answerid, PDO::PARAM_INT );
            break;
        case 'blogvote':
            $query = "INSERT INTO blogvotes ( entryid, ip, browser, fbid ) VALUES ( :entryid, :ip, :browser, :fbid)";
            $PDO = Database::$connection->prepare( $query );
            break;
        case 'entrycomment':
            $query = "INSERT INTO commentvotes ( entryid, ip, browser, fbid, commentid ) VALUES ( :entryid, :ip, :browser, :fbid, :commentid)";
            $PDO = Database::$connection->prepare( $query );
            $PDO->bindParam( ':commentid', $commentid, PDO::PARAM_INT );
            break;
        case 'entryanswer':
            $query = "INSERT INTO answervotes ( entryid, ip, browser, fbid, commentid, answerid ) VALUES ( :entryid, :ip, :browser, :fbid, :commentid, :answerid)";
            $PDO = Database::$connection->prepare( $query );
            $PDO->bindParam( ':commentid', $commentid, PDO::PARAM_INT );
            $PDO->bindParam( ':answerid', $answerid, PDO::PARAM_INT );
            break;
        case 'entryvote':
            $query = "INSERT INTO votes ( entryid, ip, browser, fbid ) VALUES ( :entryid, :ip, :browser, :fbid)";
            $PDO = Database::$connection->prepare( $query );
            break;
        default:
            # code...
            break;
    }

        $PDO->bindParam( ':entryid', $entryid, PDO::PARAM_INT );
        $PDO->bindParam( ':ip', $ip );
        $PDO->bindParam( ':browser', $browser );
        $PDO->bindParam( ':fbid', $fbid );

        $PDOsuccess = $PDO->execute();

        if($PDOsuccess){
            return true;
        } else
            return false;
        }
}


function getAllEntries(){
    return 5;
}

function getCompEntries($amount, $startAt, $sortBy, $category, $featured, $single){
    $queryBase = "SELECT entries.id, entries.featured, entries.title, entries.category, entries.youtubeid, excerpt, members.firstname, members.lastname, entries.active as 'entryactive', entries.created as 'entrycreated', votes, comments";
    if($featured){
        $query = $queryBase . " FROM entries LEFT JOIN descriptions ON entries.id = descriptions.entryid LEFT JOIN entriesmetadata ON entries.id = entriesmetadata.entryid LEFT JOIN members ON entries.memberid = members.id WHERE entries.active = 'yes' AND entries.featured = 'yes' GROUP BY entries.id ORDER BY entries.created DESC";
        $PDO = Database::$connection->prepare( $query );
        $PDO->execute();

        $results = $PDO->fetchAll();
    } else if($single){
        $query = $queryBase . " FROM entries LEFT JOIN votes ON entries.id = votes.entryid WHERE entries.active = 'yes' AND entries.id = :entryid GROUP BY entries.id";
        $PDO = Database::$connection->prepare( $query );

        $PDO->bindParam( ':entryid', $single, PDO::PARAM_INT );

        $PDO->execute();

        $results = $PDO->fetchAll();
    } else if($sortBy == 'random-in-category'){
        $amount = (int)$amount;
        $startAt = (int)$startAt;
        $category = (int)$category;

        $querystart = $queryBase . " FROM entries LEFT JOIN votes ON entries.id = votes.entryid WHERE entries.active = 'yes'";

        switch($category){
            case('1'):
                $query = $querystart . " AND categoryid = 1 GROUP BY entries.id ORDER BY RAND() LIMIT :startAt, :amount";
                break;
            case('2'):
                $query = $querystart . " AND categoryid = 2 GROUP BY entries.id ORDER BY RAND() LIMIT :startAt, :amount";
                break;
            case('3'):
                $query = $querystart . " AND categoryid = 3 GROUP BY entries.id ORDER BY RAND() LIMIT :startAt, :amount";
                break;
            case('4'):
                $query = $querystart . " AND categoryid = 4 GROUP BY entries.id ORDER BY RAND() LIMIT :startAt, :amount";
                break;
            default:
                $query = $querystart . " GROUP BY entries.id ORDER BY  RAND() LIMIT :startAt, :amount";
                break;
        }

        $PDO = Database::$connection->prepare( $query );

        $PDO->bindParam( ':startAt', $startAt, PDO::PARAM_INT );
        $PDO->bindParam( ':amount', $amount, PDO::PARAM_INT );

        $PDO->execute();

        $results = $PDO->fetchAll();
    } else{
        $amount = (int)$amount;
        $startAt = (int)$startAt;
        $category = (int)$category;

        $querystart = $queryBase . " FROM entries LEFT JOIN files ON entries.id = files.entryid LEFT JOIN entriesmetadata ON entries.id = entriesmetadata.entryid LEFT JOIN members ON entries.memberid = members.id WHERE entries.active = 'yes'";

        switch($sortBy){
            case('cat1'):
                $query = $querystart . " AND categoryid = 1 GROUP BY entries.id ORDER BY entries.created DESC LIMIT :startAt, :amount";
                break;
            case('cat2'):
                $query = $querystart . " AND categoryid = 2 GROUP BY entries.id ORDER BY entries.created DESC LIMIT :startAt, :amount";
                break;
            case('cat3'):
                $query = $querystart . " AND categoryid = 3 GROUP BY entries.id ORDER BY entries.created DESC LIMIT :startAt, :amount";
                break;
            case('cat4'):
                $query = $querystart . " AND categoryid = 4 GROUP BY entries.id ORDER BY entries.created DESC LIMIT :startAt, :amount";
                break;
            case('popular'):
                $query = $querystart . " GROUP BY entries.id ORDER BY count DESC, entries.created DESC LIMIT :startAt, :amount";
                break;
            case('random'):
                $query = $querystart . " GROUP BY entries.id ORDER BY RAND() LIMIT :startAt, :amount";
                break;
            default:
                $query = $querystart . " GROUP BY entries.id ORDER BY entries.created DESC LIMIT :startAt, :amount";
                break;
        }

        $PDO = Database::$connection->prepare( $query );

        $PDO->bindParam( ':startAt', $startAt, PDO::PARAM_INT );
        $PDO->bindParam( ':amount', $amount, PDO::PARAM_INT );

        $PDO->execute();

        $results = $PDO->fetchAll();
    }

    //$comments = getCountedComments();
    //$sortedComments = array();

    //$files = getAllFiles();
    //$sortedFiles = array();

    // foreach($files as $file){
    //     $sortedFiles[$file->entryid][] = $file->filename;
    // }
    //
    // $sortedResults = array();
    //
    // foreach($results as $result){
    //     $data = array();
    //     $data['id'] = $result->id;
    //     $data['files'] = $sortedFiles[$result->id];
    //     $data['comments'] = $sortedComments[$result->id];
    //     $data['votes'] = $result->count;
    //     $data['title'] = $result->title;
    //     $data['firstname'] = $result->firstname;
    //     $data['lastname'] = $result->lastname;
    //     $data['description'] = $result->description;
    //     $data['youtubeid'] = $result->youtubeid;
    //     $data['featured'] = $result->featured;
    //     $data['categoryid'] = $result->categoryid;
    //     $sortedResults[] = $data;
    // }

    return $results;
}


function getCountedComments(){
    $query = "SELECT entryid, COUNT(entryid) as count FROM comments WHERE comments.active = 'yes' GROUP BY entryid";

    $PDO = Database::$connection->prepare( $query );

    $PDO->bindParam( ':startAt', $startAt, PDO::PARAM_INT );
    $PDO->bindParam( ':amount', $amount, PDO::PARAM_INT );

    $PDO->execute();

    return $PDO->fetchAll();
}

function getAllFiles(){
    $query = "SELECT filename, entryid FROM files";

    $PDO = Database::$connection->prepare( $query );

    $PDO->bindParam( ':startAt', $startAt, PDO::PARAM_INT );
    $PDO->bindParam( ':amount', $amount, PDO::PARAM_INT );

    $PDO->execute();

    return $PDO->fetchAll();
}

function getShareData($entryid){
    $query = "SELECT title, firstname, lastname, entries.id as eid, youtubeid, filename FROM entries LEFT JOIN files ON entries.id = files.entryid WHERE entries.active = 'yes' AND entries.id = :entryid GROUP BY entries.id";

    $PDO = Database::$connection->prepare( $query );

    $PDO->bindParam( ':entryid', $entryid, PDO::PARAM_INT );

    $PDO->execute();

    return $PDO->fetchAll();
}

function getSingleEntry($entryid){
    $query = "SELECT entries.id, entries.title, entries.youtubeid, filename, description, entries.firstname, entries.lastname, entries.active as 'active', entries.created as 'entrycreated', COUNT(votes.entryid) as count, COUNT(comments.entryid) as comments FROM entries LEFT JOIN votes ON entries.id = votes.entryid LEFT JOIN files ON entries.id = files.entryid LEFT JOIN comments ON entries.id = comments.entryid WHERE entries.active = 'yes' AND entries.id = :entryid GROUP BY entries.id";

    $PDO = Database::$connection->prepare( $query );

    $PDO->bindParam( ':entryid', $entryid, PDO::PARAM_INT );

    $PDO->execute();

    return $PDO->fetchAll();
}

function getShortText($text, $length){
    $excerpt = substr($text, 0, $length);

    if(strlen(htmlentities($excerpt)) < 1){
        $excerpt = substr($text, 0, $length + 1);
    }

    if(strlen($excerpt) > ($length - 1)){
        $excerpt = $excerpt . "...";
    }
    return $excerpt;
}

function getFiles($entryid){
    $query = "SELECT * FROM files WHERE entryid = :entryid";

    $PDO = Database::$connection->prepare( $query );

    $PDO->bindParam( ':entryid', $entryid, PDO::PARAM_INT );

    $PDO->execute();

    return $PDO->fetchAll();
}

function getComments($entryid){
    $query = "SELECT * FROM comments WHERE entryid = :entryid AND active = 'yes'";

    $PDO = Database::$connection->prepare( $query );

    $PDO->bindParam( ':entryid', $entryid, PDO::PARAM_INT );

    $PDO->execute();

    return $PDO->fetchAll();
}

function getCommentLikes($entryid){
    $query = "SELECT COUNT(*) as count, commentid FROM commentvotes WHERE entryid = :entryid GROUP BY commentid";

    $PDO = Database::$connection->prepare( $query );

    $PDO->bindParam( ':entryid', $entryid, PDO::PARAM_INT );

    $PDO->execute();

    return $PDO->fetchAll();
}

function getBlogCommentLikes($entryid){
    $query = "SELECT COUNT(*) as count, commentid FROM blogcommentvotes WHERE entryid = :entryid GROUP BY commentid";

    $PDO = Database::$connection->prepare( $query );

    $PDO->bindParam( ':entryid', $entryid, PDO::PARAM_INT );

    $PDO->execute();

    return $PDO->fetchAll();
}

function getAnswerLikes($entryid){
    $query = "SELECT COUNT(*) as count, answerid FROM answervotes WHERE entryid = :entryid GROUP BY answerid";

    $PDO = Database::$connection->prepare( $query );

    $PDO->bindParam( ':entryid', $entryid, PDO::PARAM_INT );

    $PDO->execute();

    return $PDO->fetchAll();
}

function getBlogAnswerLikes($entryid){
    $query = "SELECT COUNT(*) as count, answerid FROM bloganswervotes WHERE entryid = :entryid GROUP BY answerid";

    $PDO = Database::$connection->prepare( $query );

    $PDO->bindParam( ':entryid', $entryid, PDO::PARAM_INT );

    $PDO->execute();

    return $PDO->fetchAll();
}

function getBlogComments($entryid){
    $query = "SELECT * FROM blogcomments WHERE entryid = :entryid AND active = 'yes'";

    $PDO = Database::$connection->prepare( $query );

    $PDO->bindParam( ':entryid', $entryid, PDO::PARAM_INT );

    $PDO->execute();

    return $PDO->fetchAll();
}

function getAllBlogComments(){
    $query = "SELECT COUNT(*) as comments, entryid FROM blogcomments WHERE active = 'yes' GROUP BY entryid";

    $PDO = Database::$connection->prepare( $query );

    $PDO->execute();

    return $PDO->fetchAll();
}

function getAnswers($entryid){
    $query = "SELECT * FROM answers WHERE entryid = :entryid AND active = 'yes'";

    $PDO = Database::$connection->prepare( $query );

    $PDO->bindParam( ':entryid', $entryid, PDO::PARAM_INT );

    $PDO->execute();

    return $PDO->fetchAll();
}

function getBlogAnswers($entryid){
    $query = "SELECT * FROM bloganswers WHERE entryid = :entryid AND active = 'yes'";

    $PDO = Database::$connection->prepare( $query );

    $PDO->bindParam( ':entryid', $entryid, PDO::PARAM_INT );

    $PDO->execute();

    return $PDO->fetchAll();
}

function getBlogVotes($entryid){
    $query = "SELECT COUNT(*) as votes FROM blogvotes WHERE entryid = :entryid";

    $PDO = Database::$connection->prepare( $query );

    $PDO->bindParam( ':entryid', $entryid, PDO::PARAM_INT );

    $PDO->execute();

    return $PDO->fetchAll();
}

function getAllBlogVotes(){
    $query = "SELECT COUNT(*) as votes, entryid FROM blogvotes GROUP BY entryid";

    $PDO = Database::$connection->prepare( $query );

    $PDO->execute();

    return $PDO->fetchAll();
}

function getRandomCategoryImage($cat){
    //cat1 = Upplevelse
    //cat2 = Salong
    //cat3 = Biosnacks
    //cat4 = Övrigt
    switch ($cat) {
        case '1':
            $images = Array('upplevelse1.jpg', 'upplevelse2.jpg', 'upplevelse3.jpg', 'upplevelse4.jpg', 'upplevelse5.jpg');
            $img = $images[array_rand($images)];
            break;
        case '2':
            $images = Array('biograf1.jpg', 'biograf2.jpg', 'biograf3.jpg', 'biograf4.jpg', 'biograf5.jpg');
            $img = $images[array_rand($images)];
            break;
        case '3':
            $images = Array('biosnacks1.jpg', 'biosnacks2.jpg', 'biosnacks3.jpg', 'biosnacks4.jpg', 'biosnacks5.jpg', 'biosnacks6.jpg', 'biosnacks7.jpg', 'biosnacks8.jpg', );
            $img = $images[array_rand($images)];
            break;
        case '4':
            $images = Array('ovrigt1.jpg', 'ovrigt2.jpg', 'ovrigt3.jpg', 'ovrigt4.jpg', 'ovrigt5.jpg');
            $img = $images[array_rand($images)];
            break;

        default:
            # code...
            break;
    }

    return "img/categoryimg/" . $img;
}

function setSession($data, $sessionPrefix){
    $_SESSION[$sessionPrefix . '-loggedin'] = true;
    $_SESSION[$sessionPrefix . '-email'] = $data['email'];
    $_SESSION[$sessionPrefix . '-name'] = $data['firstname'];
    $_SESSION[$sessionPrefix . '-id'] = $data['id'];
    $_SESSION[$sessionPrefix . '-employ'] = $data['sfemployee'];
}
