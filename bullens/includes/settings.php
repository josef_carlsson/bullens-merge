<?php

session_start();

$development = true;
$sessionPrefix = "";
$page = "";
if($development){
	$dbServer = "localhost";
	$dbName = "bullens-data";
	$dbUser = "root";
	$dbPass = "root";
	$GLOBALS['siteurl'] = 'http://localhost:8888/bullens';
	$loginURL = $GLOBALS['siteurl'] . '/sfadmin/login.php';
} else{
	$dbServer = "";
	$dbName = "";
	$dbUser = "";
	$dbPass = "";
	$GLOBALS['siteurl'] = 'http://www.bullens.se';
	$output_dir = $_SERVER["DOCUMENT_ROOT"] . "/uploads/";
}

//$adminLoginURL = $siteURL . '/admin/login.php';
//$adminURL = $siteURL . '/admin/index.php';
include ( 'Database.class.php' );
Database::connect( $dbServer, $dbUser, $dbPass, $dbName );

include ( 'Mobile_Detect.php' );
$mobileDetect = new Mobile_Detect();
$isMobile = $mobileDetect->isMobile();

include( 'functions.php' );

if(!isset($_SESSION)){
  session_start();
}
