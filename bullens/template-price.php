<?php
/* Template Name: prise */

get_header();

	$decodedUrl = base64_decode($_GET['v']);
	parse_str($decodedUrl, $output);
	$price = checkIfPriceIsValid($output['userId'], $output['code'], $output['ip']);
	$_SESSION["price"] = $price;
	$_SESSION['winnercode'] = $output['code'];
	$couponID = getTypeOfCoupon();
/*

  curl_setopt($process, CURLOPT_URL, $createUrl);
  curl_setopt($process, CURLOPT_RETURNTRANSFER, 1 );
  curl_setopt($process, CURLOPT_POST, 1);
  curl_setopt($process, CURLOPT_POSTFIELDS, json_encode($create));
  curl_setopt($process, CURLOPT_HTTPHEADER, $headers);
  $return = curl_exec($process);
  $response = json_decode($return, true );
  curl_close($process);



  */
  if(!$price['collected']){
?>
<main role="main" class="mainWrapper price">
	<!-- section -->


	<script type="application/javascript" src="http://jsonip.appspot.com/?callback=getip"> </script>
	<!-- top hero image section -->
	<?php
	$playerPrize;
	switch ($price['winning']) {
		case '1':
			$playerPrize = "Lager";
			break;
		case '2';
			$playerPrize = "IPA";
			break;
		case '3';
		  $playerPrize = "Stout";
		break;
		default:
			$playerPrize = 'Bullens Beer Banger';
		break;
	}
	?>



	<section class="section span_12_of_12 getPrizeContainer ">
		<div class="videoContainer" style="display:block;">
			<iframe class="videoPlayer" src="http://c9019.cloudnet.cloud/video/" allowfullscreen="" scrolling="no"> <!--width="400" height="300"-->
			 <p>Your browser does not support iframes.</p>
		 </iframe>
		</div>

		<section class="darkModal price">
			<section class="heroContent--Txt" >
				<?php  if($couponID == 948) { ?>
					<h1 class="heroHeader price-value" id="getPrizeHeader">Gött, en <?php echo $playerPrize; ?></h1>
					<p class="" id="getPrize-Txt">Grattis, allt du behöver göra nu är att fylla i ditt telefonnummer och trycka på knappen. En kupong kommer att skickas till din lur och korvarna är så gott som dina. Hoppas det smakar.</p>

				<?php } else { ?>
					<h1 class="heroHeader price-value" id="getPrizeHeader">Gött, du får en <?php echo $playerPrize; ?> vid köp av valfri Beer Banger</h1>
					<p class="" id="getPrize-Txt">Grattis, allt du behöver göra nu är att fylla i ditt telefonnummer och trycka på knappen. En kupong kommer att skickas till din lur. Hoppas det smakar.</p>

				<?php } ?>
				<a href="http://c9019.cloudnet.cloud/foodtruck/"><button class="heroBtn" id="heroBtn-compete">Tävla</button></a>
				<form class="prizeFormPhone" id="sendCoupon">
					<input
						data-type="phone"
						id="inputPhone"
						class="inputField val"
						type="tel"
						placeholder="Telefonnummer"
						/>
					<input type="submit" class="heroBtn" id="prizePhoneBtn" value="skicka vinst"></input>
				</form>
			</section>
		</section>
	</section>
</main>

<?php
	}
	else{
		?>
		<main role="main" class="mainWrapper price">
		<section class="darkModal">
			<section class="heroContent--Txt" >
				<h1 class="heroHeader" id="getPrizeHeader">Du har redan hämtat ut ditt pris</h1>
			</section>
		</section>
	</section>
		</main>
	<?php
	}
	?>



<?php get_footer(); ?>
