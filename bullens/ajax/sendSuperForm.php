<?php include('../includes/settings.php');

$success = false;
$data = array();

if ( !empty( $_POST ) ) {
    $errors = array();
	$code = $_SESSION["code"];
	$day = $_POST['day'];
	$motivation = $_POST['motivation'];
	$name = $_POST['name'];
	$phone = $_POST['phone'];
	$adress = $_POST['adress'];
	$city = $_POST['city'];
	$postcode = $_POST['postcode'];
	
	$submited = SuperFormSubmit($code, $day, $motivation, $name, $phone, $adress, $city, $postcode);
	
    if(!empty($submited)){
        $arr=array('status'=>'success','message'=>'started successfuly');
        echo json_encode($arr);
        exit();
    } else{
        $arr=array('status'=>'fail','message'=>'Något gick fel.');
        echo json_encode($arr);
        exit();
    }
}
