<?php include('../includes/settings.php');
$success = false;
$data = array();

if ( !empty( $_POST ) ) {
    $errors = array();
    $code = getCode();
    $_SESSION["code"] = $code;
	$browser = $_POST['browser'];
    if(empty($errors)){
        $returnData = registerInQueue($code, $browser);
    }
    if(!empty($returnData)){
        $arr=array('status'=>'success','message'=>'Registration successful');
        echo json_encode($arr);
        exit();
    } else{
        $arr=array('status'=>'fail','message'=>'Något gick fel.');
        echo json_encode($arr);
        exit();
    }
}
