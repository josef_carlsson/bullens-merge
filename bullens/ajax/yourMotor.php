<?php include('../includes/settings.php');
$success = false;
$data = array();

if ( !empty( $_POST ) ) {
    $errors = array();
	$ip = $_POST['ip'];
	$code = $_SESSION["code"];
	$returnData = createWinner($ip, $code);
	$_SESSION["id"] = $returnData;
	$motor = turnOnMotor($ip, $code, $returnData);
	
    if(!empty($motor)){
        $arr=array('status'=>'success','message'=>'started successfuly');
        echo json_encode($arr);
        exit();
    } else{
        $arr=array('status'=>'fail','message'=>'Något gick fel.');
        echo json_encode($arr);
        exit();
    }
}