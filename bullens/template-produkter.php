<?php
/* Template Name: Produkter */

get_header();

$productLoop = new WP_Query(array('post_type' => 'products') );
$counter = 0;
?>

<main role="main" class="mainWrapper">
	<section class="pageContentWrapper--no-img">


	<section class="section productsWrapper span_12_of_12">
		<?php
		if($productLoop->have_posts() ) :
		  while($productLoop->have_posts() ) : $productLoop->the_post();
				if(get_field('produktbild')) :
					$productImg = get_field('produktbild')['url'];
				endif;
				if(get_field('produktintro_kort')) :
					$productInfo = get_field('produktintro_kort');
				endif;
				if(get_field('produktintro_lanktext')) :
					$linkTxt = get_field('produktintro_lanktext');
					else :
					$linkTxt = 'Läs mer';
				endif;
				if($counter == 0) {
					$imgClass = 'imgLeft';
					$txtClass = 'imgRight';
					$counter++;
				} elseif($counter == 1) {
					$imgClass = 'imgRight';
					$txtClass = 'imgLeft';
					$counter = 0;
				}
		?>

		<section class="section span_6_of_12 prodImgContainer--all <?php echo $imgClass; ?>">
			<a href="<?php the_permalink(); ?>" ><img src="<?php echo $productImg; ?>"  class="productImg--all"/></a>
		</section>
		<section class="section span_6_of_12 prodTxtContainer--all">
			<div class="puffContent">
				<h2 class="puffHeader"><a href="<?php the_permalink(); ?>"><?php echo get_the_title();?></a></h2>
				<article>
					<?php echo $productInfo; ?>
					<span>
						<a href="<?php the_permalink(); ?>" class=""><?php echo $linkTxt;?></a>
					</span>
				</article>
			</div>

			<!-- <p class="puffContent"> -->


			<!-- </p> -->
		</section>

	<?php endwhile;
	endif;
?>
	</section>
	</section>
</main>

<?php get_footer(); ?>
