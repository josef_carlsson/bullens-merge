(function ($, root, undefined) {

	$(function () {

		'use strict';

// Mobile menu
		$('#mobile-menu').click(function() {
			if($('#mobile-menu').hasClass('menu-active')) {
				$('#mobile-menu').removeClass('menu-active');
				$('.menu').css('opacity', '0');
				setTimeout(function() {
					$('.menu').css('display', 'none');
				}, 300);
			} else {
				$('#mobile-menu').addClass('menu-active');
				$('.menu').css('display', 'block');
				setTimeout(function() {
					$('.menu').css('opacity', '1');
				}, 50);
			}

		});

// Slider start page
		var current;
		var totalSlides = $('.topSlider').children().length;

		if($(window).width() < 600 && totalSlides > 1) {
			setInterval(function() {
				next();
			}, 5000);
		}
		if(totalSlides == 1) {
		  $('.slideSelector').css('display', 'none');
		}
		var container = $('<div class="numDiv" />');

		for(var i = 0; i < totalSlides; i++) {
			var id = i + 1;
			if(id == 1) {
				container.append('<span class="sliderNum active" id="slideNum'+ id + '">' + id + '</span>');
			} else {
			  container.append('<span class="sliderNum" id="slideNum'+ id + '">' + id + '</span>');
			}
		}

		$('.numContainer').html(container);

		$('.sliderNum').on('click touch', function(e) {

			var clickedNum = e.target.id;
			console.log(clickedNum);
			var lastChar = parseInt(clickedNum.slice(-1));
			current = parseInt($('.activeSlide').attr('id'));
			if(lastChar !== current) {
				var lastCharId = '#' + lastChar;
				var currentId = '#' + current;
				var clickedNumId = '#' + clickedNum;
				var notActive = '#slideNum' + current;

				$(lastCharId).removeClass('inactiveSlide');
				$(currentId).removeClass('activeSlide');
				$(notActive).removeClass('active');

				$(currentId).addClass('inactiveSlide');
				$(lastCharId).addClass('activeSlide');
				$(clickedNumId).addClass('active');
			}
		});

		// Event listeners
		$('.prev').on('click touch', function() {
			prev();
		})
		$('.next').on('click touch', function() {
			next();
		});

		//Previous slide
		function prev() {
			current = parseInt($('.activeSlide').attr('id'));
			var prev;

			if(current == 1) {
			  prev = totalSlides;
			} else {
				prev = current - 1;
			}

			var prevId = '#' + prev;
			var currentId = '#' + current;
			var notActive = '#slideNum' + current;
			var clickedNumId = '#slideNum' + prev;

			$(prevId).removeClass('inactiveSlide');
			$(currentId).removeClass('activeSlide');
			$(notActive).removeClass('active');

			$(currentId).addClass('inactiveSlide');
			$(prevId).addClass('activeSlide');
			$(clickedNumId).addClass('active');
		}

		//Next slide
		function next() {
			current = parseInt($('.activeSlide').attr('id'));
			var next;

			if(current == totalSlides) {
				next = 1;
			} else {
				next = current + 1;
			}

			var nextId = '#' + next;
			var currentId = '#' + current;
			var notActive = '#slideNum' + current;
			var clickedNumId = '#slideNum' + next;

			$(nextId).removeClass('inactiveSlide');
			$(currentId).removeClass('activeSlide');
			$(notActive).removeClass('active');

			$(currentId).addClass('inactiveSlide');
			$(nextId).addClass('activeSlide');
			$(clickedNumId).addClass('active');
		}

		if($('body').hasClass('historia')) {
			setTimeout(function() {
				var elem = document.querySelector('.grid');
				var msnry = new Masonry( elem, {
				// options
					itemSelector: '.grid-item',
					columnWidth: '.span_6_of_12',
					percentPosition: true
				});
			}, 100);
		}



		//Validation for input Superpriset
		if($('body').hasClass('page-template-template-superpriset')) {
			var textarea = $('textarea[name="motivation"]');
			var motivation = false;
			var email = $('input[type="email"]');
			var emailValid = false;
			var inputElems = $('input[type="text"].inputField--superPrize');
			var y;
			//Set y axis for goToByScroll
			if($(window).width() < 374) {
				y = 500;
			} else {
				y = 300;
			}

			$(textarea).on('focusout', function() {
				console.log($(this).val().length);
				if(textarea.val().length < 10) {
					textarea.addClass('error');
					motivation = false;
					console.log(motivation)
				} else if(textarea.val().length >= 10) {
					motivation = true;
					console.log(motivation);

					if(textarea.hasClass('error')) {
						textarea.removeClass('error');
					}
				}
			});

			$(email).on('focusout', function() {
				var emailaddress = email.val();
				emailValid = validateEmail(emailaddress);
				console.log(emailValid)
				if(emailValid == false) {
					$(email).addClass('error');
				} else if (emailValid == true) {
					if($(email).hasClass('error')) {
						$(email).removeClass('error');
					}
				}
			});

			$(inputElems).each(function() {
				$(this).on('focusout', function() {
					if($(this).val().length < 3) {
						$(this).addClass('error');
					} else if($(this).val().length >=2) {
						if($(this).hasClass('error')) {
							$(this).removeClass('error');
						}
						$(this).removeClass('unvalid');
					}
				});
			});
			function checkValidElems() {
				var noError = true;
				$(inputElems).each(function() {
					if($(this).hasClass('unvalid')) {
						console.log($(this));
						noError = false;
					}
				});
				return noError;
			}

			$('#btn--superPrize').on('click touch', function(e) {
				e.preventDefault();
				console.log('click btn');
				var checkedElems = checkValidElems();
				if(motivation == false || checkedElems == false || emailValid == false) {
					console.log('errorrrrrrrrr')
					$('#infoForm').append('<p style="margin-top:28px;color:#e80031;">Du måste fylla i hela formuläret</p>');

				} else if(motivation == true && checkedElems == true && emailValid == true) {
					$('.formContainer--super').css('display', 'none');
					$('.formConfirmation').css('display', 'block');
					console.log('ser fint ut');

					goToByScroll('.formContainer--super', y);
				}
			})
		}

		//Validate email address
		function validateEmail(email) {
			var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
			if(reg.test(email)) {
				return true;
			} else {
				return false;
			}
		}
		function goToByScroll(container, y) {
		  var offset = $(container).offset();
			var scrolltoY = offset.top;
			$('html,body').animate({ scrollTop: scrolltoY - y } , 500);
		}

	});

})(jQuery, this);
