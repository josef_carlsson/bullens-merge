(function ($, root, undefined) {

	$(function () {

		'use strict';
		var interval = 1000;
		var ip;
		var superPriceData = [];
		var clicked = false;
				console.log(window.siteURL);
		function getSuperPriceInputValue(){
			superPriceData = {};
			$('.val').each(function(index,item){
				var type = $(item).data('type');
				var val = $(item).val();
				superPriceData[type] = val;
			});	
			
			return superPriceData;
		}
		    $('.price form').on('submit', function(e) { //use on if jQuery 1.7+
		        e.preventDefault();  //prevent form from submitting
				var data = getSuperPriceInputValue();
				jQuery.ajax({
	            type: "POST",
	            dataType:"json",
	            url: window.siteURL + "ajax/coupon.php",
	            data: data,
	            success:function(response){
		            	console.log(response)
		            	if(response == true){
			            	$('.val').val('');
							$('.price form').remove();
							$('.price-value').html('TACK')
		            	}
		            	else{
		            		$('.price-value').append('något gick fel')
		            	}
		            },
		            error:function (xhr, ajaxOptions, thrownError){
		              	console.log(xhr);
		              	console.log(thrownError);
		            }
		    	})
		    });
		    

			  $('.super-price').on('submit', function(e) { //use on if jQuery 1.7+
		        e.preventDefault();  //prevent form from submitting
				var superdata = getSuperPriceInputValue();
				console.log(superdata);
				jQuery.ajax({
	            type: "POST",
	            url: window.siteURL + "ajax/sendSuperForm.php",
	            data: superdata,
	            success:function(response){
		            	console.log(response);	
		            	$('.super-price input, textarea').val('')
		            },
		            error:function (xhr, ajaxOptions, thrownError){
		              	console.log(xhr);
		              	console.log(thrownError);
		            }
		    	});
					
		    });
		    $('body').on('click', '.calendar button' , function(e){
			    e.preventDefault();
			    $('.calendar button').removeClass('val');
			    $(this).addClass('val')
		    });
		    
		    $('body').on('click', '#place-in-queue', function(){
			    	//$('body').addClass('fortune-1');
					//$('.logo').attr('src', window.directoryUri + '/img/logo/logo-bullens_140.png');
					$('.videoContainer').css('display', 'block');
					$('.heroContent').css('display', 'none');
						$('.stream').addClass('wheel');
						    registerInQueue();
		    getPlaceInQueue();
				$(this).remove();
			});
		    $('body').on('click', '#removeFromQueue', function(){
				removeOldQueue();
			});
		
			$('body').on('click', '#removeFromQueue', function(){
				removeOldQueue();
			});
			$('body').on('click', '#startMotor', function(){
				clicked = true;
				$('#startMotor').attr('disabled','disabled');
				startMotor();
			});
		
		$.get("http://ipinfo.io", function(response) {
		    ip = response.ip;
		}, "jsonp");
		
		
		function checkWinning() {
			var myData = {
				'ip': ip
			};
			jQuery.ajax({
	            type: "POST",
	            url: window.siteURL + "ajax/collectWinning.php",
	            data: myData,
	            success:function(response){
		            if(response.length > 5){
			        window.location.href = window.siteURL + '../../../price/?v=' + response;
		                
		            }
	            	console.log(response);	
	            },
	            error:function (xhr, ajaxOptions, thrownError){
	              	console.log(xhr);
	              	console.log(thrownError);
	            }
	    	});
	    }
	    if($('.wheel').length > 0){

	    	// if less then ... setInterval(checkMotor, 1000);
	    }
	    
		function startMotor(){
			var myData = {
				'ip': ip
			};
			jQuery.ajax({
	            type: "POST",
	            url: window.siteURL + "ajax/yourMotor.php",
	            dataType:"json",
	            data: myData,
	            success:function(response){
	            	console.log(response);
					$('#startMotor').remove();
	            	setInterval(checkWinning, 1000);
	            },
	            error:function (xhr, ajaxOptions, thrownError){
		          	$('#startMotor').removeAttr('disabled');
	              	console.log(xhr);
	              	console.log(thrownError);
	            }
	    	});
	    }
	    var sec = 15;	
		function getreminingtime(){
			sec = sec - 1;
			console.log(sec);
			if(sec >= 0 && clicked == false){
			setTimeout(function(){
				getreminingtime()
				}, 1000)
				$('#timer').text(sec);
			}
			else{
				$('#timer').text('running');
			}
			return sec; 
		}
	    function removeMefromQueue(){
		    if(clicked == false){
				$.get(window.siteURL + "ajax/removeMeFromQueue.php", function(data) { 								
					 console.log(data);
					 $('#startMotor').remove();
					 
					 var content;
					 content = '<h1 class="heroHeader">Too slow</h1>';
					 content += '<p>Tiden gick ut, nu är det någon annans tid att snurra hjulet.</p>';
					 $('#livestream').html(content);
				});
			}
	    }	
	    function checkMotor(){
			 $.get(window.siteURL + "ajax/checkMotor.php", function(data) { 								
				 console.log(data);
				 });
	    	}		
	    function getPlaceInQueue(){
			 $.get(window.siteURL + "ajax/getPlaceInQueue.php", function(data) { 
				
				var content					
				 interval = data * 1000;
				 if(interval >= 10000){
					 interval = 10000
				 }
				 if(isNaN(interval)){
					 content = data;
					 content +=  '<h3 id="timer"></h3>';
					  getreminingtime()
					 setTimeout(function(){
						 
						 removeMefromQueue();
					 }, 15000)
				 }
				 else{
					content = '<h3> din plats är nr ' + data + '</h3>';
					content += '<p> det är ungefär : ' + (data * 15) + ' sekunder</p>';
					 setTimeout(function(){
						console.log(interval);
						
						getPlaceInQueue()
					 }, interval);
				 }
				  $('#livestream').html(content); 
				 

				 });
	    	}		
	    
		function removeOldQueue(){
			var myData = {
				'ip': ip
			};
			jQuery.ajax({
	            type: "POST",
	            url: window.siteURL + "ajax/removeOldQueue.php",
	            dataType:"json",
	            data: myData,
	            success:function(response){
	            	console.log(response);
	            },
	            error:function (xhr, ajaxOptions, thrownError){
	              	console.log(xhr);
	              	console.log(thrownError);
	            }
	    	});
	    }	
	    	
		function removeOldQueue(){
			var myData = {
				'ip': ip
			};
			jQuery.ajax({
	            type: "POST",
	            url: window.siteURL + "ajax/removeOldQueue.php",
	            dataType:"json",
	            data: myData,
	            success:function(response){
	            	console.log(response);
	            },
	            error:function (xhr, ajaxOptions, thrownError){
	              	console.log(xhr);
	              	console.log(thrownError);
	            }
	    	});
	    }			

		function registerInQueue(){
			var browser = bowser.name + " " + bowser.version;
			var myData = {
				'browser': browser
			};
			jQuery.ajax({
	            type: "POST",
	            url: window.siteURL + "ajax/registerInQueue.php",
	            dataType:"json",
	            data: myData,
	            success:function(response){
	            	console.log(response);
	            },
	            error:function (xhr, ajaxOptions, thrownError){
	              	console.log(xhr);
	              	console.log(thrownError);
	            }
	    	});
	    }
		function makeActive(){
			//removeOldQueue
		}
		var superPrizeHeader = '<h1 class="heroHeader" id="getPrizeHeader">Kupongen är på väg. Vill du vinna mer?</h1>'
		var superPrizeContent = '<p class="timesUp">Tävla om en riktig korvfest med Bullens Beer Banger. Vinn Bullens Food Truck, fullastad av gratis korv med smak av öl hem till dig <a href="http://c9019.cloudnet.cloud/superpriset/">här</a>.</p>';
		var superPrizeBtn = 'Tävla';

		$('#prizePhoneBtn').on('click touch', function() {
			console.log($('#inputPhone').length);

			$('.prizeFormPhone').css('display', 'none');
			$('#getPrizeHeader').html(superPrizeHeader);
			$('#getPrize-Txt').html(superPrizeContent);
			$('#heroBtn-compete').css('display', 'block');
			$('#heroBtn-compete').html(superPrizeBtn);
		});
	});

})(jQuery, this);
