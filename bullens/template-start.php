<?php

/* Template Name: Startsida  */

get_header();

?>

	<main role="main" class="mainWrapper">
		<!-- section -->
		<?php
		if(get_field('toppbild')) :
			if(have_rows('toppbild')) :
				$id = 0;
				?>
				<ul class="topSlider">

				<?php
				while(have_rows('toppbild')) : the_row();
				$id++;
				$heroImg = get_sub_field('bild');
				$heroText = get_sub_field('bildtext');
				if(get_sub_field('lank')) {
					$heroCTAlink = get_sub_field('lank');
				}
				// if(get_sub_field('lanktext')) {
				// 	$heroCTAtxt = get_sub_field('lanktext');
				// }
				if($id == 1 ) {
					$class = 'activeSlide';
				} else {
					$class = 'inactiveSlide';
				}
				?>
				<li class="<?php echo $class; ?>" id="<?php echo $id; ?>">
					<section class="section span_12_of_12">
						<div class="topImgContainer" style="background-image:url('<?php echo $heroImg; ?>')"></div>
						<div class="section span_6_of_12 pageHeaderContainer ">
							<h1 class="pageHeader">
							<?php if(get_sub_field('lank')) { ?>
								<a href="<?php echo $heroCTAlink; ?>"><?php echo $heroText; ?></a>
							<?php } else {
								echo $heroText;
							}?>
							</h1>
						</div>


					</section>
				</li>
				<?php endwhile; ?>

				</ul>
				<div class="">
					<div class="slideSelector">
						<span class="prev"></span>
						<span class="numContainer"></span>
						<span class="next"></span>
					</div>
				</div>

			<?php endif;
		endif;
		?>

		<section class="section puffWrapper span_12_of_12">
		<?php
		$counter = 0;
		if(get_field('puff')) :
			if(have_rows('puff')) :
				$allPosts = [];
				while (have_rows('puff')) : the_row();

				$title = get_sub_field('titel');
				$text = get_sub_field('text');
				$subFieldImg = get_sub_field('bild');
				$img = $subFieldImg;
				if(get_sub_field('lank')) :
					$link = get_sub_field('lank');
				endif;
				if(get_sub_field('lanktext')) :
				  $linkTxt = get_sub_field('lanktext');
					else :
					$linkTxt = 'Läs mer';
				endif;
				if($counter == 0) {
					$imgClass = 'imgLeft';
					$counter++;
				} elseif ($counter == 1) {
					$imgClass = 'imgRight';
					$counter = 0;
			}
				?>



				<section class="section span_6_of_12 puffTxtContainer">
					<div class="puffContent">
						<h2 class="puffHeader"><?php echo $title;?></h2>
						<article class="puffDivider">
							<?php echo $text;
							if(get_sub_field('lank')) :
							?>
								<span><a href="<?php echo $link; ?>" target="_blank"><?php echo $linkTxt; ?> </a></span>
							<?php endif; ?>
						</article>
					</div>
				</section>
				<section class="section span_6_of_12 puffImgContainer <?php echo $imgClass; ?>" style="background-image:url('<?php echo $img; ?>');"></section>
				<?php endwhile;
			endif;
		endif;
		?>
		</section>
	</main>

<?php get_footer(); ?>
