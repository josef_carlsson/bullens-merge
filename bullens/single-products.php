<?php get_header(); ?>

<main role="main" class="mainWrapper">
	<?php
	$currentPost = get_the_ID();
	$heroText = get_the_title();
	$productIntro = get_field('produktintroduktion');
	$productIngredients = get_field('ingredienser');
	$productNutrition = get_field('naringsvarden');
	$productAllergies = get_field('allergifakta');
	$productImg = get_field('produktbild')['url'];
	$inspirationImg = get_field('inspirationsbild');
	?>

	<section class="pageContentWrapper--no-img">
		<section class="section span_12_of_12 productWrapper">
			<div class="rel">
			  <section class="section span_6_of_12 bg" style="background-image:url('<?php echo $inspirationImg; ?>');"></section>
				<section class="section span_6_of_12 txtContainer--singleRight">
					<div class="puffContent single">
					  <h2 class="puffHeader"><?php echo $heroText;?></h2>
						<article><?php echo $productIntro; ?></article>
					</div>
				</section>
			</div>
			<section class="section span_6_of_12 productImgContainer imgRight">
				<a href="<?php the_permalink(); ?>"><img src="<?php echo $productImg; ?>" class="productImg"/></a>
			</section>
			<section class="section span_6_of_12 prodTxtContainer--all singleLeft">
				<div class="puffContent facts">
					<?php if(get_field('ingredienser')) : ?>
						<h3 class="puffHeader">Ingredienser</h3>
						<p><?php echo $productIngredients; ?></p>
					<?php endif ?>

					<?php if(get_field('naringsvarden')) : ?>
						<h3 class="puffHeader">Näringsvärden</h3>
						<p><?php echo $productNutrition; ?></p>
					<?php endif ?>

					<?php if(get_field('allergifakta')) : ?>
						<h3 class="puffHeader">Allergifakta</h3>
						<p><?php echo $productAllergies; ?></p>
					<?php endif ?>
				</div>


			</section>
		</section><!-- productWrapper -->

	<?php
		$productLoop = new WP_Query(array('post_type' => 'products') );
	?>

	<div class="maxWidth">
		<section class="section span_12_of_12 allProducts">
	    <div></div>
			<div></div>
			<?php
			$counter = 0;
			if($productLoop->have_posts() ) :
				while ($productLoop->have_posts() ) : $productLoop->the_post();
				  $productImg = get_field('produktbild')['url'];

					if(get_field('produktnamn')) {
						$productName = get_field('produktnamn');
					} else {
						$productName = get_the_title();
					}

					if($currentPost !== $id) {?>
						<section class="otherProductContainer">
							<a href="<?php the_permalink(); ?>"><img src="<?php echo $productImg; ?>" class="otherProductImg" /></a>
							<span class="otherProductTitle"><?php echo $productName; ?></span>
						</section>
					<?php }

				?>

				<?php
				endwhile;
			endif;
				?>
		</section><!-- allProducts -->
	</div> <!-- maxWidth -->
</section> <!-- Page content wrapper -->

</main>



<?php get_footer(); ?>
